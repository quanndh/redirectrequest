const express = require('express');
const bP = require('body-parser');
const request = require('./request')

const app = express();

app.use(bP.json());

app.post("/send", async (req, res) => {
    console.log("ds")
    try {
        let options = req.body;
        if (options.body) options.body = JSON.stringify(options.body)
        let rs = await request.send(options);
        res.send(rs)
    } catch (error) {
        console.log(error)
    }
})

let port = process.env.PORT || 4444;
app.listen(port, err => {
    if (err) console.log(err)
    else console.log('server start at', port)
})