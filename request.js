const request = require('request');
module.exports = {
  send: async function (options) {
    process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = 0;
    let rs = await new Promise((resolve, reject) => {
      request(options, (error, response, body) => {
        try {
          if (error) reject(error);
          console.log("0", response)
          resolve(response);
        } catch (err) {
          console.log("1", err)
          reject(err);
        }
      });
    });
    return rs;
  },
};
